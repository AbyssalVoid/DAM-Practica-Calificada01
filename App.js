import 'react-native-gesture-handler';

import React from 'react';

import MainStackNavigator from './componentes/screens/mainStackNavigation';

export default function App() {
  return <MainStackNavigator />
}