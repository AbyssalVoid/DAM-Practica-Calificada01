import React from 'react'
import { StyleSheet, View, Text, Image, ImageBackground } from 'react-native'
import Icons from '../img/export';

function Settings(props) {
  const { route, navigation } = props
  const { actor } = route.params

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.fondo} source={ require('../img/Background.png')}>
        <Text style={styles.slogan}>PROJECT TWILIGHT</Text>
      <View style={styles.actorCont}>
        <View style={{flex: 1,flexDirection: 'row'}}>
          <View style={{flex: 1,flexDirection: 'column'}}>
            <Text style={styles.title}>{actor.name}</Text>
            <Text style={styles.text}>{actor.des}</Text>
            <Text style={styles.text}>Clase: {actor.clase}</Text>
            <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/attack.png')}
              />
              <Text style={styles.stat}>+ {actor.stats[0]}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/defense.png')}
              />
                <Text style={styles.stat}>+ {actor.stats[1]}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/magicattack.png')}
              />
              <Text style={styles.stat}>+ {actor.stats[2]}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/magicdeffense.png')}
              />
              <Text style={styles.stat}>+ {actor.stats[3]}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/heart.png')}
              />
              <Text style={styles.stat}>+ {actor.stats[4]}</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Image style={styles.icon}
                  source={require('../img/mp.png')}
              />
              <Text style={styles.stat}>+ {actor.stats[5]}</Text>
          </View> 
          </View>
          <Image style={styles.avatar} source={Icons[actor.img]} />
        </View>
          
      </View>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebebeb'
  },
  slogan: {
    color: '#f8f8ff',
    fontSize: 36,
    fontStyle: 'italic', backgroundColor: '#4b0082',
    textAlign: 'center', fontWeight: 'bold'
  },
  title: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold'
  },
  text: {
    color: '#101010',
    fontSize: 16,
    marginBottom: 10
  },
  actorCont: { 
    flex: 1,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 16,
    backgroundColor: '#f8f8ff',
    padding: 10,
}, 
fondo: {
  padding: 15,
  flex: 1,
    resizeMode: "cover"
} ,
icon: {height: 25, width: 25},
stat: {
  fontSize: 16,
  fontWeight: 'bold',
  alignItems: 'flex-end'
},
  avatar: {
    flex: 1,
    backgroundColor: "#add8e6",
    borderWidth: 10,
    borderRadius: 20,
    width: 225,
    height: 250
  }
})

export default Settings