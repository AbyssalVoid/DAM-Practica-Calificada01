import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, SafeAreaView, FlatList } from 'react-native'
import Data from '../data/actors.json'
import Icons from '../img/export';

function Detail(props) {
        
  const Actors = Data
  const { navigation } = props;
  return (
    <View style={styles.contenedor}>
      <Text style={styles.titulo}>Lista de personajes de ProjectTwilight</Text>
        <SafeAreaView  style={styles.container}>  
            <FlatList  
                data={Actors}
                renderItem={({ item }) => (
                    <View
                        key={item.key}>
                        <View style={styles.container}>
                            <Image
                                style={styles.tinyLogo}
                                source={Icons[item.img]}
                            />
                            <View style={{flex:1, flexDirection: 'column', paddingLeft: 10}}>
                            <TouchableOpacity onPress={() => navigation.navigate('Char', { actor: item })}>
                                <Text style={styles.titulo}
                                onPress={() => navigation.navigate('Char', { actor: item })}>{item.name}</Text>
                            </TouchableOpacity>                               
                                <Text style={styles.desc}>{item.des}</Text>
                            </View>
                        </View>
                    </View>
                )}
                keyExtractor={item => item.id}
            />  
        </SafeAreaView >
    </View>
  )
}

const styles = StyleSheet.create({
  container: {  
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    margin: 5,
    backgroundColor: '#f5f5f5',
    borderColor: 'gray', borderWidth: 1,
  },  
  titulo: {  
      fontWeight: 'bold',
      fontSize: 30,
      textAlign: 'center',
      marginBottom: 5
  },
  desc: {
      fontSize: 20,
      textAlign: 'justify'
  },
  tinyLogo: {
      width: 120,
      height: 120,
  },
  contenedor: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ebebeb'
  },
  text: {
    color: '#ffffff',
    fontSize: 24,
    fontWeight: 'bold'
  },
  card: {
    width: 350,
    height: 100,
    borderRadius: 10,
    backgroundColor: '#101010',
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  cardText: {
    fontSize: 18,
    color: '#ffd700',
    marginBottom: 5
  },
  buttonContainer: {
    backgroundColor: '#222',
    borderRadius: 5,
    padding: 10,
    margin: 20
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  }
})

export default Detail