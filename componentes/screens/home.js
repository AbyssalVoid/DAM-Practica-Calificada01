import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, ImageBackground } from 'react-native'

function Home(props) {
  const { navigation } = props
  return (
    <View style={styles.container}>
      <ImageBackground style={styles.fondo}  source={ require('../img/Background.png')} blurRadius={2}>
      <Text style={{color: '#101010',
                    fontSize: 40,
                    fontWeight: 'bold'}}>ProjectTwilight</Text>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('List')}>
        <Text style={styles.buttonText}>Ver lista de personajes</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('Item')}>
        <Text style={styles.buttonText}>Ver lista de items</Text>
      </TouchableOpacity>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebebeb'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold'
  },
  buttonContainer: {
    backgroundColor: '#222',
    borderRadius: 5,
    padding: 10,
    margin: 20
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  },
  fondo: {
    flex: 1,
    
    justifyContent: 'center',
    alignItems: 'center',
      resizeMode: "cover"
  } 
})

export default Home